using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
  public class RoadMapsRepository : IRoadMapsRepository
  {
    private readonly AppIdentityDbContext _context;
    public RoadMapsRepository(AppIdentityDbContext context)
    {
      _context = context;
    }

    public async Task<string> CreateRoadMaps(RoadMaps RoadMaps)
    {
      try
      {
        _context.RoadMaps.Add(RoadMaps);
        var check = await _context.SaveChangesAsync();
        return "Success";
      }
      catch
      {
        return "Failed";
      }
    }

    public async Task<string> UpdateRoadMaps(RoadMaps RoadMaps)
    {
      _context.Entry(RoadMaps).State = EntityState.Modified;
      if (!RoadMapsExists(RoadMaps.Id))
      {
        return "RoadMaps doesn't exist";
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }

    public async Task<string> DeleteRoadMaps(string id)
    {
      var RoadMaps = await _context.RoadMaps.FindAsync(id);
      
      if (!RoadMapsExists(RoadMaps.Id))
      {
        return "RoadMaps doesn't exist";
      }
      try
      {
        _context.RoadMaps.Remove(RoadMaps);
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return "Failed";
      }
      return "Success";
    }

    public async Task<List<RoadMaps>> GetAllRoadMapsOfCourse(string idCourse)
    {
      return await _context.RoadMaps.Where(e => e.idCourse == idCourse).OrderBy(x => x.createdAt).ToListAsync();
    }
    public async Task<RoadMaps> FindById(string id)
    {
      if (RoadMapsExists(id) == true)
      {
        var RoadMaps = await _context.RoadMaps.FindAsync(id);
        return RoadMaps;
      }
      else
      {
        return null;
      }
    }
    private bool RoadMapsExists(string id)
    {
      return _context.RoadMaps.Any(e => e.Id == id);
    }

    public async Task<List<RoadMaps>> FindByTitle(string searchRoadMaps, int pageSize, int pageNumber)
    {
      return await _context.RoadMaps.Where(c => c.title.Contains(searchRoadMaps)).OrderByDescending(x => x.createdAt).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
    }
  }
}