using System;
using Core.Entities;
using Core.Entities.Identity;

namespace Core.Dto
{
    public class BackStudentScoreOfCourseDto
    {

        public string id { get; set; }
        public string idCourse { get; set; }
        public Courses Courses { set; get; }
        public string idUser { set; get; }
        public AppUser Student { set; get; }
        public string linkAvatarStudent {set; get;}
        public string idAssignment { set; get; }
        public CreateAssignments CreateAssignment { set; get; }
        public string idPointer { set; get; }
        public AppUser User { set; get; }
        public float score{set;get;}

        public string urlAssignment { set; get; }


    }
}
