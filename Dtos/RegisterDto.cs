using System.ComponentModel.DataAnnotations;
namespace Core.Dto
{
    public class RegisterDto
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{6,}$", ErrorMessage = "Password must have 1 Uppercase, 1 Lowercase, 1 number and at least 6 characters ")]
        public string Password { get; set; }
        [Required]
        public string DisplayName { get; set; }
        public string PhoneNumber { get; set; }

        public string Address {get; set;}
    }
}
