using System;
using System.ComponentModel.DataAnnotations;
namespace Core.Dto
{
    public class RoadMapContentDto
    {
        [StringLength(36)]
        public string idRoadMap { get; set; }

        [Required]
        [StringLength(120)]
        public string title { get; set; }

        [Required]
        [StringLength(120)]
        public string content { get; set; }

    }
}
