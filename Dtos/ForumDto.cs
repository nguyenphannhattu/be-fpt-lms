using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace identity.Dto
{
    public class ForumsDto
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        public string Content { get; set; }
        public IFormFile UrlImage { get; set; }

    }
}
