using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace identity.Dto
{
    public class CourseDto
    {
        [StringLength(36)]
        public string idTeacher { get; set; }
        [StringLength(36)]
        public string idClassAdmin { get; set; }
        [Required]
        [StringLength(50)]
        public string name { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        public int duration { get; set; }
        
        public IFormFile URLImage { get; set; }

    }
}
