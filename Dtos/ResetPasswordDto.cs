using System.ComponentModel.DataAnnotations;
namespace identity.Dto
{
  public class ResetPasswordDto
  {
    [Required]
    public string token { get; set; }
    [Required]
    public string email { get; set; }
    [Required]
    [DataType(DataType.Password)]
    public string NewPassword { get; set; }
    [DataType(DataType.Password)]
    [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
    public string ConfirmNewPassword { get; set; }

  }
}
