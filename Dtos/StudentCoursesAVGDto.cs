using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities;
using Core.Entities.Identity;

namespace identity.Dto
{
    public class StudentCoursesAVGDto
    {
        [Key]
        public string idCourse { get; set; }
        [Key]
        public string idUser { get; set; }
        [ForeignKey("idUser")]
        public AppUser Student { set; get; }
        public string status { get; set; }
        public float aVGScore { get; set; }
        public bool isMentor {get;set;}

    }
}