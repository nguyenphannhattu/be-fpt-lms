using System;

namespace Core.Dto
{
    public class DeadLineDto
    {
        // public string idAssigment { get; set; }
        // public string idCourse { get; set; }
        // public string nameAssigment { get; set; }
        // public string nameCourse { get; set; }
        public string Id { get; set; }
        public string nameCourse { get; set; }
        public string title { set; get; }
        public DateTime start { set; get; }
        public DateTime finish { set; get; }

    }
}
