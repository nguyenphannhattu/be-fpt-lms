using System.ComponentModel.DataAnnotations;
namespace identity.Dto
{
    public class ForgetPasswordDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        
    }
}
