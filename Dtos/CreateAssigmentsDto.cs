using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace identity.Dto
{
    public class CreateAssigmentsDto
    {
        [StringLength(36)]
        public string idRoadMapContent { get; set; }
        [Required]
        [StringLength(120)]
        public string title { get; set; }
        [Required]
        [StringLength(320)]
        public string content { get; set; }
        public string start { get; set; }
        public string finish { get; set; }
        public IFormFile URLFile { get; set; }

    }
}
