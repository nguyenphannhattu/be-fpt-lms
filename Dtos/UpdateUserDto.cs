using Microsoft.AspNetCore.Http;

namespace Core.Dto
{
    public class UpdateUserDto
    {
        public string DisplayName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public IFormFile avatar {get;set;}
    }
}
