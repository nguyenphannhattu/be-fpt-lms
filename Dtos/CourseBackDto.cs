using System;
using System.ComponentModel.DataAnnotations;
using Core.Entities.Identity;
using Microsoft.AspNetCore.Http;

namespace identity.Dto
{
  public class CourseBackDto
  {
    public string Id { get; set; }

    public string idInstructor { get; set; }
    public AppUser Instructor { set; get; }


    public string idTeacher { get; set; }
    public AppUser Teacher { set; get; }

    public string idClassAdmin { get; set; }
    public AppUser ClassAdmin { set; get; }

    public string name { get; set; }

    public string description { get; set; }
    public string status { get; set; }

    public int duration { get; set; }

    public string URLLink { get; set; }

  }
}
