using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Entities
{
    public class BaseTime
    {
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }

    }
}