using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Entities
{
    public class BaseEntity : BaseTime
    {
        [Key]
        public string Id { get; set; }

    }
}