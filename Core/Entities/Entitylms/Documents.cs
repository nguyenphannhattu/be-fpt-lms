using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities.Identity;

namespace Core.Entities
{
    public class Documents : BaseEntity
    {
        public string roadmapContentId { get; set; }
        [ForeignKey("roadmapContentId")]
        public RoadMapContents RoadMapContent { set; get; }
        public string idCreator { get; set; }
        [ForeignKey("idCreator")]
        public AppUser Teacher { set; get; }
        public string title { set; get; }
        public string content { set; get; }
        public string urlDocument { get; set; }

    }
}