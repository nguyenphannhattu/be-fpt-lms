using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities.Identity;

namespace Core.Entities
{
    public class EveluateCourses : BaseEntity
    {

        public string idCourse { get; set; }
        [ForeignKey("idCourse")]
        public Courses Course { set; get; }
        public string idUser { get; set; }
        [ForeignKey("idUser")]
        public AppUser User { set; get; }
        public string comment { get; set; }
        public float point{set;get;}

    }
}