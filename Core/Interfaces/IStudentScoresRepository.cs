using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Core.Interfaces
{
    public interface IStudentScoresRepository
    {
        Task<string> CreateStudentScores(StudentScores studentScores);
        Task<string> UpdateStudentScores(StudentScores studentScores);
        Task<StudentScores> FindById(string idCourse, string idUser, string idAssignment);
        Task<float> GetAVGScoreOfStudent(string idCourse, string idStudent);
         Task<List<StudentScores>> GetScoreByCourse(string idCourse, int pageSize, int pageNumber);
         Task<List<StudentScores>> GetScoreByStudent(string idStudent, int pageSize, int pageNumber);
         Task<List<StudentScores>> GetScoreByAssignment(string idAssignment, int pageSize, int pageNumber);
         Task<List<StudentScores>> GetScoreByStudentOfCourse(string idCourse, string idStudent, int pageSize, int pageNumber);
  }
}