using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Core.Interfaces
{
    public interface ICommentRepository
    {
        Task<List<Comments>> GetAllCommentAsync(string idForum);
        Task<string> CreateComment(Comments comments );
        Task<string> UpdateComment(Comments comments );

        Task<Comments> FindById(string id);
        // Task<List<Comments>> FindByName(string searchCourse);
  }
}