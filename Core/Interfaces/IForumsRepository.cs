using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Identity;
using identity.Dto;

namespace Core.Interfaces
{
    public interface IForumsRepository
    {
        Task<List<Forums>> GetAllForumsAsync(int pageSize, int pageNumber);
        Task<string> CreateForums(Forums forum);
        Task<string> UpdateForums(Forums forum);
        Task<Forums> FindById(string id);
        Task<List<Forums>> FindByTitle(string searchforum, int pageSize, int pageNumber);
  }
}