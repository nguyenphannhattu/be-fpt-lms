using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Extensions;
using AutoMapper;
using Core.Dto;
using Core.Entities;
using Core.Entities.Identity;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace identity.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class RoadMapContentsController : ControllerBase
  {
    private readonly IRoadmapContentRepository _roadMapContentsRepository;
    private readonly IRoadMapsRepository _roadMapsRepository;
    private readonly UserManager<AppUser> _userManager;
    private readonly ICreateAssigmentsRepository _createAssigmentsRepository;
    private readonly IDocumentsRepository _documentRepository;
    private readonly ICourseRepository _courseRepository;
    private readonly IStudentCoursesRepository _studentCourseRepository;
    private readonly IUploadService _uploadService;
    private readonly IMapper _mapper;

    public RoadMapContentsController(IRoadmapContentRepository roadMapContentsRepository, IRoadMapsRepository roadMapsRepository, UserManager<AppUser> userManager, ICreateAssigmentsRepository createAssigmentsRepository, IDocumentsRepository documentRepository, ICourseRepository courseRepository, IStudentCoursesRepository studentCoursesRepository, IUploadService uploadService, IMapper mapper)
    {
      _roadMapContentsRepository = roadMapContentsRepository;
      _roadMapsRepository = roadMapsRepository;
      _userManager = userManager;
      _createAssigmentsRepository = createAssigmentsRepository;
      _documentRepository = documentRepository;
      _courseRepository = courseRepository;
      _studentCourseRepository = studentCoursesRepository;
      _uploadService = uploadService;
      _mapper = mapper;
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("getRoadMapContent")]
    public async Task<ActionResult<RoadMapContents>> getRoadMapContent([FromQuery] string id, [FromQuery] int pageNumber, [FromQuery] int pageSize)
    {
      try
      {
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var targetTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Asia/Ho_Chi_Minh");
        var timeCurrent = TimeZoneInfo.ConvertTime(DateTime.Now, targetTimeZone);
        var roadMapContent = await _roadMapContentsRepository.FindById(id);
        if (roadMapContent != null)
        {
          ReturnRoadMapContentDto roadMapContentResult = _mapper.Map<ReturnRoadMapContentDto>(roadMapContent);
          var listAssigments = await _createAssigmentsRepository.GetAllAssignmentsByRoadMapContent(id);
          List<CreateAssigmentsBackDto> listAssigmentsBack = _mapper.Map<List<CreateAssigmentsBackDto>>(listAssigments);
          roadMapContentResult.createAssignment = listAssigmentsBack;
          foreach (var itemAssignment in listAssigmentsBack)
          {
            if (timeCurrent >= itemAssignment.start && timeCurrent <= itemAssignment.finish)
            {
              itemAssignment.outofdate = false;
            }
            else
            {
              itemAssignment.outofdate = true;
            }
            var resultGetLink = _uploadService.GetLinkAssigment(itemAssignment.urlFile);
            itemAssignment.urlFile = resultGetLink.linkUrl;
          }
          var listDocument = await _documentRepository.GetAllDocumentsByRoadMapContent(id, pageSize, pageNumber);
          roadMapContentResult.documents = listDocument;
          foreach (var itemDocument in listDocument)
          {
            var resultGetLink = _uploadService.GetLinkDocument(itemDocument.urlDocument);
            itemDocument.urlDocument = resultGetLink.linkUrl;
          }
          var result = new
          {
            Message = "Get RoadMapContent Success",
            data = roadMapContentResult,
            Success = true,
          };
          return Ok(result);
        }
        return BadRequest(new ApiResponse(400, "Get RoadMapContent Failed"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }


    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("getAllRoadMapContentsOfRM/{idRoadMap}")]
    public async Task<ActionResult<List<RoadMapContents>>> GetAllRoadMapContentsOfRM(string idRoadMap, [FromQuery] int pageNumber, [FromQuery] int pageSize)
    {
      try
      {
        if (pageSize < 8 || pageSize > 20)
        {
          pageSize = 8;
        }
        if (pageNumber < 1)
        {
          pageNumber = 1;
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var roleUser = await _userManager.GetRolesAsync(user);
        var roadMap = await _roadMapsRepository.FindById(idRoadMap);
        var courses = await _courseRepository.FindById(roadMap.idCourse);
        var studentCourses = await _studentCourseRepository.FindByIdCourseAndIdUser(roadMap.idCourse, user.Id);
        if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher)
        || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor)
        || (roleUser[0] == "CLASSADMIN" && user.Id == courses.idClassAdmin)
        || (roleUser[0] == "STUDENT" && studentCourses != null))
        {
          var listRoadMapContents = await _roadMapContentsRepository.GetAllRoadMapContentsOfRM(idRoadMap, pageSize, pageNumber);
          List<ReturnRoadMapContentDto> listReturnRoadMapContents = _mapper.Map<List<ReturnRoadMapContentDto>>(listRoadMapContents);
          var targetTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Asia/Ho_Chi_Minh");
          var timeCurrent = TimeZoneInfo.ConvertTime(DateTime.Now, targetTimeZone);
          foreach (var item in listReturnRoadMapContents)
          {
            var listAssigments = await _createAssigmentsRepository.GetAllAssignmentsByRoadMapContent(item.Id);
            List<CreateAssigmentsBackDto> listAssigmentsBack = _mapper.Map<List<CreateAssigmentsBackDto>>(listAssigments);
            item.createAssignment = listAssigmentsBack;
            foreach (var itemAssignment in listAssigmentsBack)
            {
              if (timeCurrent >= itemAssignment.start && timeCurrent <= itemAssignment.finish)
              {
                itemAssignment.outofdate = false;
              }
              else
              {
                itemAssignment.outofdate = true;
              }
              var resultGetLink = _uploadService.GetLinkAssigment(itemAssignment.urlFile);
              itemAssignment.urlFile = resultGetLink.linkUrl;
            }
            var listDocument = await _documentRepository.GetAllDocumentsByRoadMapContent(item.Id, pageSize, pageNumber);
            item.documents = listDocument;
            foreach (var itemDocument in listDocument)
            {
              var resultGetLink = _uploadService.GetLinkDocument(itemDocument.urlDocument);
              itemDocument.urlDocument = resultGetLink.linkUrl;
              itemDocument.RoadMapContent = null;
            }
          }
          var result = new
          {
            Message = "Get All RoadMapContents Success",
            data = listReturnRoadMapContents,
            Success = true,
          };
          if (listReturnRoadMapContents == null)
          {
            return BadRequest(new ApiResponse(404, "No data found!"));
          }
          return Ok(result);
        }
        else
        {
          return BadRequest(new ApiResponse(400, "Bạn không có quyền truy cập!"));
        }
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPost("createRoadMapContents")]
    public async Task<ActionResult<RoadMapContents>> CreateRoadMapContents(RoadMapContentDto roadmapContentDto)
    {

      try
      {
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);

        var roleUser = await _userManager.GetRolesAsync(user);
        var checkRoadMap = await _roadMapsRepository.FindById(roadmapContentDto.idRoadMap);
        if (checkRoadMap == null)
          return BadRequest(new ApiResponse(404, "Do Not RoadMap"));
        var courses = await _courseRepository.FindById(checkRoadMap.idCourse);
        if (courses == null)
          return BadRequest(new ApiResponse(404, "Do Not Course"));
        if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher) || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor))
        {
          Guid id = Guid.NewGuid();
          var roadMapContents = new RoadMapContents()
          {
            Id = id.ToString(),
            idRoadMap = roadmapContentDto.idRoadMap,
            title = roadmapContentDto.title,
            content = roadmapContentDto.content,
            createdAt = DateTime.Now,
            updatedAt = DateTime.Now,
          };
          var roadMapContentsResult = await _roadMapContentsRepository.CreateRoadMapContent(roadMapContents);
          if (roadMapContentsResult == "Success")
          {
            var result = new
            {
              Message = "Create RoadMapContents Success",
              data = roadMapContents,
              Success = true,
            };
            return Ok(result);
          }
        }
        return BadRequest(new ApiResponse(400, "Create Failed RoadMapContents"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPut("updateRoadMapContents/{id}")]
    public async Task<ActionResult<RoadMapContents>> UpdateRoadMapContents(string id, RoadMapContentDto roadmapContentDto)
    {
      try
      {
        //Lấy thông tin RoadMap
        var roadMapofRMC = await _roadMapsRepository.FindById(roadmapContentDto.idRoadMap);
        var roadMaps = new RoadMaps()
        {
          Id = roadMapofRMC.Id,
          idCourse = roadMapofRMC.idCourse,
          title = roadMapofRMC.title,
          createdAt = roadMapofRMC.createdAt,
          updatedAt = roadMapofRMC.updatedAt,
        };
        if (roadMaps == null)
        {
          return BadRequest(new ApiResponse(404, "RoadMap doesn't exist! Please create a RoadMap first!"));
        }
        var roadMapContentsCurrent = await _roadMapContentsRepository.FindById(id);
        if (roadMapContentsCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "RoadMapContent doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var courses = await _courseRepository.FindById(roadMaps.idCourse);
        var roleUser = await _userManager.GetRolesAsync(user);
        if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher) || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor))
        {
          roadMapContentsCurrent.idRoadMap = roadMaps.Id;
          roadMapContentsCurrent.title = roadmapContentDto.title;
          roadMapContentsCurrent.content = roadmapContentDto.content;
          roadMapContentsCurrent.updatedAt = DateTime.Now;
          var roadMapContentsResult = await _roadMapContentsRepository.UpdateRoadMapContent(roadMapContentsCurrent);

          if (roadMapContentsResult == "Success")
          {
            var result = new
            {
              Message = "Update RoadMap Success",
              data = roadMapContentsCurrent,
              Success = true,
            };
            return Ok(result);
          }
          else if (roadMapContentsResult == "RoadMapContent doesn't exist")
          {
            return BadRequest(new ApiResponse(404, "RoadMapContent doesn't exist"));
          }
        }
        return BadRequest(new ApiResponse(400, "Update Failed RoadMapContent"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpDelete("deleteRoadMapContents/{id}")]
    public async Task<ActionResult<RoadMapContents>> DeleteRoadMapContents(string id)
    {
      try
      {
        var roadMapContentsCurrent = await _roadMapContentsRepository.FindById(id);
        if (roadMapContentsCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "RoadMapContent doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var roadMap = await _roadMapsRepository.FindById(roadMapContentsCurrent.idRoadMap);
        var courses = await _courseRepository.FindById(roadMap.idCourse);
        var roleUser = await _userManager.GetRolesAsync(user);
        if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher) || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor))
        {
          roadMapContentsCurrent.updatedAt = DateTime.Now;
          Console.WriteLine(roadMapContentsCurrent);
          var roadMapContentsResult = await _roadMapContentsRepository.DeleteRoadMapContent(id);
          if (roadMapContentsResult == "Success")
          {
            var result = new
            {
              Message = "Delete RoadMapContent Success",
              Success = true,
            };
            return Ok(result);
          }
          else if (roadMapContentsResult == "RoadMapContent doesn't exist")
          {
            return BadRequest(new ApiResponse(404, "RoadMapContent doesn't exist"));
          }

        }
        return BadRequest(new ApiResponse(400, "Delete Failed RoadMapContent"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }

    }

  }
}
