using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Extensions;
using AutoMapper;
using Core.Dto;
using Core.Entities;
using Core.Entities.Identity;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace identity.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentScoresController : ControllerBase
    {
        private readonly IStudentScoresRepository _studentScores;
        private readonly IMentorCoursesRepository _mentorCourseRepository;
        private readonly IStudentCoursesRepository _studentCourseRepository;
        private readonly ICreateAssigmentsRepository _createAssigments;
        private readonly ICourseRepository _courseRepository;
        private readonly UserManager<AppUser> _userManager;
        private readonly IUploadService _uploadService;
        private readonly IMapper _mapper;

        public StudentScoresController(IStudentScoresRepository studentScoresRepository, UserManager<AppUser> userManager, ICourseRepository courseRepository, ICreateAssigmentsRepository createAssigments, IUploadService uploadService, IStudentCoursesRepository studentCoursesRepository, IMentorCoursesRepository mentorCoursesRepository, IMapper mapper)
        {
            _studentScores = studentScoresRepository;
            _userManager = userManager;
            _courseRepository = courseRepository;
            _createAssigments = createAssigments;
            _uploadService = uploadService;
            _studentCourseRepository = studentCoursesRepository;
            _mentorCourseRepository = mentorCoursesRepository;
            _mapper = mapper;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("getStudentScore")]
        public async Task<ActionResult<StudentScores>> getStudentScore([FromQuery] string idCourse, [FromQuery] string idStudent, [FromQuery] string idAssignment)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var StudentScoreResult = await _studentScores.FindById(idCourse, idStudent, idAssignment);
                var resultGetLinkAssignment = _uploadService.GetLinkAssigment(StudentScoreResult.urlAssignment);
                StudentScoreResult.urlAssignment = resultGetLinkAssignment.linkUrl;
                if (StudentScoreResult != null)
                {
                    var result = new
                    {
                        Message = "Get StudentScore Success",
                        data = StudentScoreResult,
                        Success = true,
                    };
                    return Ok(result);
                }
                return BadRequest(new ApiResponse(400, "Get StudentScore Failed"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("getLinkSubmit/{idStudent}")]
        public async Task<ActionResult<String>> GetLinkSubmit([FromQuery] string idCourse, [FromQuery] string idAssignment, string idStudent)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var studentScoresCurrent = await _studentScores.FindById(idCourse, idStudent, idAssignment);
                if (studentScoresCurrent == null)
                {
                    return BadRequest(new ApiResponse(400, "Find Failed Submit"));
                }
                var uploadService = _uploadService.GetLinkDocument(studentScoresCurrent.urlAssignment);
                if (uploadService != null)
                {
                    var result = new
                    {
                        Message = "Get Link Success",
                        data = uploadService,
                        Success = true,
                    };
                    return Ok(result);
                }
                return BadRequest(new ApiResponse(400, "Get Failed Submit"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("getAVGScoreOfStudent")]
        public async Task<ActionResult<float>> GetAVGScoreOfStudent([FromQuery] string idCourse, [FromQuery] string idStudent)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleUser = await _userManager.GetRolesAsync(user);
                var courses = await _courseRepository.FindById(idCourse);
                var studentCourses = await _studentCourseRepository.FindByIdCourseAndIdUser(idCourse, idStudent);
                if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher)
                || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor)
                || (roleUser[0] == "CLASSADMIN" && user.Id == courses.idClassAdmin)
                || (user.Id == idStudent))
                {
                    var aVG = await _studentScores.GetAVGScoreOfStudent(idCourse, idStudent);
                    var result = new
                    {
                        Message = "Get AVG Score Of Student Success",
                        data = aVG,
                        Success = true,
                    };
                    return Ok(result);
                }
                else
                {
                    return BadRequest(new ApiResponse(400, "Bạn không có quyền truy cập!"));
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("getStudentScoreOfCourses")]
        public async Task<ActionResult<List<StudentScores>>> GetListStudentScoreOfCourses([FromQuery] string idCourse, [FromQuery] string idAssignment, [FromQuery] int pageNumber, [FromQuery] int pageSize)
        {
            try
            {
                if (pageSize < 8 || pageSize > 20)
                {
                    pageSize = 8;
                }
                if (pageNumber < 1)
                {
                    pageNumber = 1;
                }
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleUser = await _userManager.GetRolesAsync(user);
                var courses = await _courseRepository.FindById(idCourse);
                var studentCourses = await _studentCourseRepository.FindByIdCourseAndIdUser(idCourse, user.Id);
                var mentorCourses = await _mentorCourseRepository.FindByIdCourseAndIdUser(idCourse, user.Id);
                if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher)
                || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor)
                || (roleUser[0] == "CLASSADMIN" && user.Id == courses.idClassAdmin)
                || (roleUser[0] == "STUDENT" && studentCourses != null && mentorCourses != null))
                {
                    var listStudentScore = await _studentScores.GetScoreByAssignment(idAssignment, pageSize, pageNumber);
                    if (idAssignment == null)
                    {
                        listStudentScore = await _studentScores.GetScoreByCourse(idCourse, pageSize, pageNumber);
                    }
                    foreach (var item in listStudentScore)
                    {
                        item.Courses = null;
                        var student = await _userManager.FindByIdAsync(item.idUser);
                        var pointer = await _userManager.FindByIdAsync(item.idPointer);
                        var assignment = await _createAssigments.FindById(item.idAssignment);
                        var resultGetLinkAssignment = _uploadService.GetLinkAssigment(item.urlAssignment);
                        item.urlAssignment = resultGetLinkAssignment.linkUrl;
                        item.CreateAssignment.RoadMapContent = null;
                        item.CreateAssignment.Teacher = null;
                    }
                    List<BackStudentScoreOfCourseDto> resultScore = _mapper.Map<List<BackStudentScoreOfCourseDto>>(listStudentScore);
                    foreach (var item in resultScore)
                    {
                        Guid id = Guid.NewGuid();
                        item.id = id.ToString();
                        if (item.Student.urlAvatar != null)
                        {
                            var resultGetLinkAvatar = _uploadService.GetLinkAvatar(item.Student.urlAvatar);
                            item.linkAvatarStudent = resultGetLinkAvatar.linkUrl;
                        }
                    }
                    var result = new
                    {
                        Message = "Get List StudentScore Success",
                        data = resultScore,
                        Success = true,
                    };
                    return Ok(result);
                }
                else
                {
                    return BadRequest(new ApiResponse(400, "Bạn không có quyền truy cập!"));
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("getStudentScoreOfStudent/{idStudent}")]
        public async Task<ActionResult<List<StudentScores>>> GetListScoreStudent(string idStudent, [FromQuery] int pageNumber, [FromQuery] int pageSize)
        {
            try
            {
                if (pageSize < 8 || pageSize > 20)
                {
                    pageSize = 8;
                }
                if (pageNumber < 1)
                {
                    pageNumber = 1;
                }
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                if (user.Id == idStudent)
                {
                    var studentScore = await _studentScores.GetScoreByStudent(idStudent, pageSize, pageNumber);
                    var result = new
                    {
                        Message = "Get List StudentScore By idStudent Success",
                        data = studentScore,
                        Success = true,
                    };
                    return Ok(result);
                }
                else
                {
                    return BadRequest(new ApiResponse(400, "Bạn không có quyền truy cập!"));
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("createStudentScores")]
        public async Task<ActionResult<StudentScores>> CreateStudentScores([FromForm] StudentScoreDto studentScoresDto)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleUser = await _userManager.GetRolesAsync(user);
                var checkCourse = await _courseRepository.FindById(studentScoresDto.idCourse);
                if (checkCourse == null)
                    return BadRequest(new ApiResponse(404, "Do Not Course"));
                var checkAssigment = await _createAssigments.FindById(studentScoresDto.idAssignment);
                if (checkAssigment == null)
                    return BadRequest(new ApiResponse(404, "Do Not Assigment"));
                var checkStudentCourses = await _studentCourseRepository.FindByIdCourseAndIdUser(studentScoresDto.idCourse, user.Id);
                if (checkStudentCourses == null)
                    return BadRequest(new ApiResponse(400, "Bạn không phải học viên của khóa học!"));
                var targetTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Asia/Ho_Chi_Minh");
                var timeCurrent = TimeZoneInfo.ConvertTime(DateTime.Now, targetTimeZone);
                if (roleUser[0] == "STUDENT" && timeCurrent >= checkAssigment.start && timeCurrent <= checkAssigment.finish)
                {
                    var resultUpload = await _uploadService.UploadSubmit(user.Id.ToString(), studentScoresDto.idAssignment.ToString(), studentScoresDto.UrlSubmit);
                    var studentScore = new StudentScores()
                    {
                        idCourse = studentScoresDto.idCourse,
                        idUser = user.Id,
                        idAssignment = studentScoresDto.idAssignment,
                        urlAssignment = resultUpload.nameFile,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now,
                    };
                    var studentScoreResult = await _studentScores.CreateStudentScores(studentScore);
                    var resultData = new
                    {
                        idCourse = studentScoresDto.idCourse,
                        idUser = user.Id,
                        idAssignment = studentScoresDto.idAssignment,
                        urlAssignment = resultUpload.nameFile,
                        linkAssignment = resultUpload.url,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now,
                    };
                    if (studentScoreResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Create StudentScores Success",
                            data = resultData,
                            Success = true,
                        };
                        return Ok(result);
                    }
                }
                return BadRequest(new ApiResponse(400, "Create Failed StudentScores"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("updateStudentScores")]
        public async Task<ActionResult<StudentScores>> UpdateStudentScores([FromForm] StudentScoreDto studentScoresDto, [FromForm] string idStudent, [FromForm] float Score)
        {
            try
            {
                var checkAssigment = await _createAssigments.FindById(studentScoresDto.idAssignment);
                if (checkAssigment == null)
                    return BadRequest(new ApiResponse(404, "Do Not Assigment"));
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleUser = await _userManager.GetRolesAsync(user);
                var courses = await _courseRepository.FindById(studentScoresDto.idCourse);
                if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher) || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor))
                {
                    var studentScoresCurrent = await _studentScores.FindById(studentScoresDto.idCourse, idStudent, studentScoresDto.idAssignment);
                    if (studentScoresCurrent == null)
                    {
                        return BadRequest(new ApiResponse(404, "StudentScore doesn't exist"));
                    }
                    var resultGetLink = _uploadService.GetLinkDocument(studentScoresCurrent.urlAssignment);
                    studentScoresCurrent.idPointer = user.Id;
                    studentScoresCurrent.score = Score;
                    studentScoresCurrent.updatedAt = DateTime.Now;
                    var studentScoresResult = await _studentScores.UpdateStudentScores(studentScoresCurrent);
                    var resultData = new
                    {
                        idCourse = studentScoresDto.idCourse,
                        idUser = idStudent,
                        idPointer = user.Id,
                        score = Score,
                        idAssignment = studentScoresDto.idAssignment,
                        urlAssignment = studentScoresCurrent.urlAssignment,
                        linkAssignment = resultGetLink.linkUrl,
                        createdAt = studentScoresCurrent.createdAt,
                        updatedAt = DateTime.Now,
                    };

                    if (studentScoresResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Update StudentScore Success",
                            data = resultData,
                            Success = true,
                        };
                        return Ok(result);
                    }
                    else if (studentScoresResult == "StudentScore doesn't exist")
                    {
                        return BadRequest(new ApiResponse(404, "StudentScore doesn't exist"));
                    }


                }
                if (roleUser[0] == "STUDENT" && idStudent != null)
                {
                    var studentScoresCurrent = await _studentScores.FindById(studentScoresDto.idCourse, idStudent, studentScoresDto.idAssignment);
                    if (studentScoresCurrent == null)
                    {
                        return BadRequest(new ApiResponse(404, "StudentScore doesn't exist"));
                    }
                    var mentorCourses = await _mentorCourseRepository.FindByIdCourseAndIdUser(studentScoresDto.idCourse, user.Id);
                    var studentCourses = await _studentCourseRepository.FindByIdCourseAndIdUser(studentScoresDto.idCourse, user.Id);
                    if (studentCourses == null)
                        return BadRequest(new ApiResponse(400, "Bạn không phải học viên của khóa học!"));
                    if (mentorCourses == null)
                        return BadRequest(new ApiResponse(400, "Bạn không phải Trợ Giảng của khóa học!"));
                    var resultGetLink = _uploadService.GetLinkDocument(studentScoresCurrent.urlAssignment);
                    studentScoresCurrent.idPointer = user.Id;
                    studentScoresCurrent.score = Score;
                    studentScoresCurrent.updatedAt = DateTime.Now;
                    var studentScoresResult = await _studentScores.UpdateStudentScores(studentScoresCurrent);
                    var resultData = new
                    {
                        idCourse = studentScoresDto.idCourse,
                        idUser = idStudent,
                        idPointer = user.Id,
                        score = Score,
                        idAssignment = studentScoresDto.idAssignment,
                        urlAssignment = studentScoresCurrent.urlAssignment,
                        linkAssignment = resultGetLink.linkUrl,
                        createdAt = studentScoresCurrent.createdAt,
                        updatedAt = DateTime.Now,
                    };

                    if (studentScoresResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Update StudentScore Success",
                            data = resultData,
                            Success = true,
                        };
                        return Ok(result);
                    }
                    else if (studentScoresResult == "StudentScore doesn't exist")
                    {
                        return BadRequest(new ApiResponse(404, "StudentScore doesn't exist"));
                    }
                }
                var targetTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Asia/Ho_Chi_Minh");
                var timeCurrent = TimeZoneInfo.ConvertTime(DateTime.Now, targetTimeZone);
                // var timeCurrent = DateTime.Now;
                if (roleUser[0] == "STUDENT" && idStudent == null && timeCurrent >= checkAssigment.start && timeCurrent <= checkAssigment.finish)
                {
                    var studentScoresCurrent = await _studentScores.FindById(studentScoresDto.idCourse, user.Id, studentScoresDto.idAssignment);
                    if (studentScoresCurrent == null)
                    {
                        return BadRequest(new ApiResponse(404, "StudentScore doesn't exist"));
                    }
                    if (studentScoresDto.UrlSubmit != null)
                    {
                        var resultUpload = await _uploadService.UploadSubmit(user.Id.ToString(), studentScoresDto.idAssignment.ToString(), studentScoresDto.UrlSubmit);
                        if (resultUpload == null)
                        {
                            return BadRequest(new ApiResponse(400, "Update Failed StudentScore"));
                        }
                        studentScoresCurrent.urlAssignment = resultUpload.nameFile;
                        studentScoresCurrent.updatedAt = DateTime.Now;
                        var studentScoresResult = await _studentScores.UpdateStudentScores(studentScoresCurrent);
                        var resultData = new
                        {
                            idCourse = studentScoresCurrent.idCourse,
                            idUser = user.Id,
                            idAssignment = studentScoresCurrent.idAssignment,
                            urlAssignment = resultUpload.nameFile,
                            linkAssignment = resultUpload.url,
                            createdAt = studentScoresCurrent.createdAt,
                            updatedAt = DateTime.Now,
                        };

                        if (studentScoresResult == "Success")
                        {
                            var result = new
                            {
                                Message = "Update StudentScore Success",
                                data = resultData,
                                Success = true,
                            };
                            return Ok(result);
                        }
                        else if (studentScoresResult == "StudentScore doesn't exist")
                        {
                            return BadRequest(new ApiResponse(404, "StudentScore doesn't exist"));
                        }
                    }
                    else
                    {
                        return BadRequest(new ApiResponse(400, "Update Failed StudentScore"));
                    }
                }

                return BadRequest(new ApiResponse(400, "Update Failed StudentScore"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }

    }
}
