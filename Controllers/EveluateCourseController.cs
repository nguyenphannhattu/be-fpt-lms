using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Extensions;
using Core.Entities;
using Core.Entities.Identity;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace identity.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class EveluateCourseController : ControllerBase
  {
    private readonly IEveluateCourseRepository _eveluateCourseRepository;
    private readonly ICourseRepository _courseRepository;

    private readonly UserManager<AppUser> _userManager;

    private readonly IUploadService _uploadService;


    public EveluateCourseController(IEveluateCourseRepository eveluateCourseRepository, UserManager<AppUser> userManager, ICourseRepository courseRepository, IUploadService uploadService)
    {
      _eveluateCourseRepository = eveluateCourseRepository;
      _userManager = userManager;
      _courseRepository = courseRepository;
      _uploadService =uploadService;
    }
    [HttpGet("getAllEveluateCourse/{id}")]
    public async Task<ActionResult<List<EveluateCourses>>> GetAllEveLuateCourses(string id)
    {
      try
      {
        var eveluates = await _eveluateCourseRepository.GetAllEveluateCoursesAsync(id);
        foreach (var item in eveluates)
        {
          var user = await _userManager.FindByIdAsync(item.idUser);
          if (user.urlAvatar != null)
          {
            var linkAvatar = _uploadService.GetLinkAvatar(user.urlAvatar);
            user.urlAvatar =linkAvatar.linkUrl;
          }
          item.User = user;
        }
        var result = new
        {
          Message = "Get All Eveluate Course Success",
          data = eveluates,
          Success = true,
        };
        return Ok(result);
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPost("createEveluateCourse")]
    public async Task<ActionResult<EveluateCourses>> CreateEveluateCourse(EveluateCourseDto eveluateCourseDto)
    {
      try
      {
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var course = await _courseRepository.FindById(eveluateCourseDto.idCourse);
        var checkEveluate = await _eveluateCourseRepository.FindByUserAndCourseAsync(user.Id, eveluateCourseDto.idCourse);
        // Chưa check khóa học đã hoàn thành
        if (checkEveluate == "Success")
        {
          return BadRequest(new ApiResponse(400, "You Eveluated Course"));
        }
        if (course != null)
        {
          Guid id = Guid.NewGuid();
          var eveluateCourse = new EveluateCourses()
          {
            Id = id.ToString(),
            idUser = user.Id,
            idCourse = course.Id,
            comment = eveluateCourseDto.comment,
            point = eveluateCourseDto.point,
            createdAt = DateTime.Now,
            updatedAt = DateTime.Now,
          };
          var eveluateResult = await _eveluateCourseRepository.CreateEveluate(eveluateCourse);
          if (eveluateResult == "Success")
          {
            var result = new
            {
              Message = "Create Eveluate Success",
              data = eveluateCourse,
              Success = true,
            };
            return Ok(result);
          }
        }
        return BadRequest(new ApiResponse(400, "Create Failed Eveluate"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPut("updateEveluateCourse/{id}")]
    public async Task<ActionResult<Courses>> UpdateCourse(string id, EveluateCourseDto eveluateCourseDto)
    {
      try
      {
        var eveluateCurrent = await _eveluateCourseRepository.FindById(id);
        var course = await _courseRepository.FindById(eveluateCourseDto.idCourse);
        if (course == null)
        {
          return BadRequest(new ApiResponse(404, "course doesn't exist"));
        }
        if (eveluateCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "Eveluate doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        if (user.Id == eveluateCurrent.idUser)
        {
          var roleInstructor = await _userManager.GetRolesAsync(user);
          eveluateCurrent.comment = eveluateCourseDto.comment;
          eveluateCurrent.point = eveluateCourseDto.point;
          eveluateCurrent.updatedAt = DateTime.Now;
          var courseResult = await _eveluateCourseRepository.UpdateEveluate(eveluateCurrent);

          if (courseResult == "Success")
          {
            var result = new
            {
              Message = "Update Eveluate Success",
              data = eveluateCurrent,
              Success = true,
            };
            return Ok(result);
          }
          else if (courseResult == "Eveluate doesn't exist")
          {
            return BadRequest(new ApiResponse(404, "Eveluate doesn't exist"));
          }
        }
        return BadRequest(new ApiResponse(400, "Update Failed Eveluate"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpDelete("deleteEveluateCourse/{id}")]
    public async Task<ActionResult<Courses>> DeleteEveluate(string id)
    {
      try
      {
        var eveluateCurrent = await _eveluateCourseRepository.FindById(id);
        if (eveluateCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "Eveluate doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var roleInstructor = await _userManager.GetRolesAsync(user);
        if (roleInstructor[0] == "INSTRUCTOR")
        {
          var courseResult = await _eveluateCourseRepository.DeleteEveluate(id);
          if (courseResult == "Success")
          {
            var result = new
            {
              Message = "Delete Eveluate Success",
              Success = true,
            };
            return Ok(result);
          }
        }
        return BadRequest(new ApiResponse(400, "Delete Failed Eveluate"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("totalEveluate/{id}")]
    public async Task<ActionResult<Courses>> TotalEveluate(string id)
    {
      try
      {
        var listEveluateCurrent = await _eveluateCourseRepository.GetAllEveluateCoursesAsync(id);
        float totalPoint = 0;
        int totalEveluate = 0;
        foreach (var item in listEveluateCurrent)
        {
          totalPoint = totalPoint + item.point;
          totalEveluate = totalEveluate + 1;
        }
        totalPoint = (float)Math.Round(totalPoint / totalEveluate, 2);
        var data = new
        {
          totalEveluate = totalEveluate,
          totalPoint = totalPoint
        };
        var result = new
        {
          Message = "Get Point Eveluate Success",
          data = data,
          Success = true,
        };
        return Ok(result);
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
  }
}
