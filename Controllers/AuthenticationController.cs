using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using API.Extensions;
using Core.Dto;
using Core.Entities.Identity;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Infrastructure.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace identity.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class AuthenticationController : ControllerBase
  {
    private readonly UserManager<AppUser> _userManager;
    private readonly SignInManager<AppUser> _signInManager;
    private readonly ITokenService _tokenService;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly IUploadService _uploadService;
    private readonly IMapper _mapper;


    public AuthenticationController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, ITokenService tokenService, RoleManager<IdentityRole> roleManager, IUploadService uploadService, IMapper mapper)
    {
      _signInManager = signInManager;
      _tokenService = tokenService;
      _userManager = userManager;
      _roleManager = roleManager;
      _uploadService = uploadService;
      _mapper = mapper;
    }
    [HttpPost("register")]
    public async Task<ActionResult<UserDto>> Register(RegisterDto register)
    {
      if (CheckEmailExistsAsync(register.Email).Result.Value || CheckUserNameAsync(register.UserName).Result.Value)
      {
        return new BadRequestObjectResult(new ApiValidationErrorResponse { Errors = new[] { "Email address is in use or Username address is in use" } });
      }
      var user = new AppUser
      {
        Email = register.Email,
        UserName = register.UserName,
        DisplayName = register.DisplayName,
        PhoneNumber = register.PhoneNumber,
        Address = register.Address,
        PhoneNumberConfirmed = true,
      };
      var result = await _userManager.CreateAsync(user, register.Password);
      if (!result.Succeeded) return BadRequest(new ApiResponse(400, "Password không hợp lệ"));
      string tokenUser = _tokenService.CreateToken(user);
      bool x = await _roleManager.RoleExistsAsync("STUDENT");
      if (!x)
      {
        var role = await _roleManager.CreateAsync(new IdentityRole { Name = "STUDENT" });
      }
      var addRoleUser = await _userManager.AddToRoleAsync(user, "STUDENT");
      if (!addRoleUser.Succeeded) return BadRequest(new ApiResponse(400, "Không thêm được role cho user"));
      //send confirmation email
      string confirmationToken = _userManager.
           GenerateEmailConfirmationTokenAsync(user).Result;
      string confirmationLink = Url.Action(nameof(ConfirmEmail),
        "Authentication", new
        {
          email = user.Email,
          token = confirmationToken
        },
         Request.Scheme);
      string content = "Nhấn vào link sau để xác nhận Email";
      SendEmail(user.Email, "Confirm Email", confirmationLink, content);
      var userIdentity = new UserDto
      {
        UserName = user.UserName,
        Email = user.Email,
        DisplayName = user.DisplayName,
        PhoneNumber = register.PhoneNumber,
        Address = register.Address,
        Token = tokenUser,
      };
      var resultRegister = new
      {
        Message = "Success Register",
        data = userIdentity,
        Success = true,
      };
      return Ok(resultRegister);
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPost("registerTeacher")]
    public async Task<ActionResult<UserDto>> RegisterTeacher(RegisterDto register)
    {
      if (CheckEmailExistsAsync(register.Email).Result.Value || CheckUserNameAsync(register.UserName).Result.Value)
      {
        return new BadRequestObjectResult(new ApiValidationErrorResponse { Errors = new[] { "Email address is in use or Username address is in use" } });
      }
      var user = new AppUser
      {
        Email = register.Email,
        UserName = register.UserName,
        DisplayName = register.DisplayName,
        PhoneNumber = register.PhoneNumber,
        Address = register.Address,
        PhoneNumberConfirmed = true,
        EmailConfirmed = true,
      };
      var result = await _userManager.CreateAsync(user, register.Password);
      if (!result.Succeeded) return BadRequest(new ApiResponse(400, "Password không hợp lệ"));
      string tokenUser = _tokenService.CreateToken(user);
      bool x = await _roleManager.RoleExistsAsync("TEACHER");
      if (!x)
      {
        var role = await _roleManager.CreateAsync(new IdentityRole { Name = "TEACHER" });
      }
      var addRoleUser = await _userManager.AddToRoleAsync(user, "TEACHER");
      if (!addRoleUser.Succeeded) return BadRequest(new ApiResponse(400, "Không thêm được role cho user"));
      //send confirmation email
      string content = "Chúc mừng bạn đã trở thành giáo viên của LMS ";
      SendEmail(user.Email, "Chào Mừng Đến Với LMS", "", content);
      var userIdentity = new TeacherDto
      {
        UserName = user.UserName,
        Email = user.Email,
        DisplayName = user.DisplayName,
        PhoneNumber = register.PhoneNumber,
        Address = register.Address,
      };
      var resultRegister = new
      {
        Message = "Success Register",
        data = userIdentity,
        Success = true,
      };
      return Ok(resultRegister);
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPost("registerInstructor")]
    public async Task<ActionResult<UserDto>> RegisterInstructor(RegisterDto register)
    {
      if (CheckEmailExistsAsync(register.Email).Result.Value || CheckUserNameAsync(register.UserName).Result.Value)
      {
        return new BadRequestObjectResult(new ApiValidationErrorResponse { Errors = new[] { "Email address is in use or Username address is in use" } });
      }
      var user = new AppUser
      {
        Email = register.Email,
        UserName = register.UserName,
        DisplayName = register.DisplayName,
        PhoneNumber = register.PhoneNumber,
        Address = register.Address,
        PhoneNumberConfirmed = true,
        EmailConfirmed = true,
      };
      var result = await _userManager.CreateAsync(user, register.Password);
      if (!result.Succeeded) return BadRequest(new ApiResponse(400, "Password không hợp lệ"));
      string tokenUser = _tokenService.CreateToken(user);
      bool x = await _roleManager.RoleExistsAsync("INSTRUCTOR");
      if (!x)
      {
        var role = await _roleManager.CreateAsync(new IdentityRole { Name = "INSTRUCTOR" });
      }
      var addRoleUser = await _userManager.AddToRoleAsync(user, "INSTRUCTOR");
      if (!addRoleUser.Succeeded) return BadRequest(new ApiResponse(400, "Không thêm được role cho user"));
      //send confirmation email
      string content = "Chúc mừng bạn đã trở thành Instructor của LMS ";
      SendEmail(user.Email, "Chào Mừng Đến Với LMS", "", content);
      var userIdentity = new TeacherDto
      {
        UserName = user.UserName,
        Email = user.Email,
        DisplayName = user.DisplayName,
        PhoneNumber = register.PhoneNumber,
        Address = register.Address,
      };
      var resultRegister = new
      {
        Message = "Success Register",
        data = userIdentity,
        Success = true,
      };
      return Ok(resultRegister);
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPost("registerClassAdmin")]
    public async Task<ActionResult<UserDto>> RegisterClassAdmin(RegisterDto register)
    {
      if (CheckEmailExistsAsync(register.Email).Result.Value || CheckUserNameAsync(register.UserName).Result.Value)
      {
        return new BadRequestObjectResult(new ApiValidationErrorResponse { Errors = new[] { "Email address is in use or Username address is in use" } });
      }
      var user = new AppUser
      {
        Email = register.Email,
        UserName = register.UserName,
        DisplayName = register.DisplayName,
        PhoneNumber = register.PhoneNumber,
        Address = register.Address,
        PhoneNumberConfirmed = true,
        EmailConfirmed = true,
      };
      var result = await _userManager.CreateAsync(user, register.Password);
      if (!result.Succeeded) return BadRequest(new ApiResponse(400, "Password không hợp lệ"));
      string tokenUser = _tokenService.CreateToken(user);
      bool x = await _roleManager.RoleExistsAsync("CLASSADMIN");
      if (!x)
      {
        var role = await _roleManager.CreateAsync(new IdentityRole { Name = "CLASSADMIN" });
      }
      var addRoleUser = await _userManager.AddToRoleAsync(user, "CLASSADMIN");
      if (!addRoleUser.Succeeded) return BadRequest(new ApiResponse(400, "Không thêm được role cho user"));
      //send confirmation email
      string content = "Chúc mừng bạn đã trở thành ClassAdmin của LMS ";
      SendEmail(user.Email, "Chào Mừng Đến Với LMS", "", content);
      var userIdentity = new TeacherDto
      {
        UserName = user.UserName,
        Email = user.Email,
        DisplayName = user.DisplayName,
        PhoneNumber = register.PhoneNumber,
        Address = register.Address,
      };
      var resultRegister = new
      {
        Message = "Success Register",
        data = userIdentity,
        Success = true,
      };
      return Ok(resultRegister);
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPost("registerSystemAdmin")]
    public async Task<ActionResult<UserDto>> RegisterSystemAdmin(RegisterDto register)
    {
      if (CheckEmailExistsAsync(register.Email).Result.Value || CheckUserNameAsync(register.UserName).Result.Value)
      {
        return new BadRequestObjectResult(new ApiValidationErrorResponse { Errors = new[] { "Email address is in use or Username address is in use" } });
      }
      var user = new AppUser
      {
        Email = register.Email,
        UserName = register.UserName,
        DisplayName = register.DisplayName,
        PhoneNumber = register.PhoneNumber,
        Address = register.Address,
        PhoneNumberConfirmed = true,
        EmailConfirmed = true,
      };
      var result = await _userManager.CreateAsync(user, register.Password);
      if (!result.Succeeded) return BadRequest(new ApiResponse(400, "Password không hợp lệ"));
      string tokenUser = _tokenService.CreateToken(user);
      bool x = await _roleManager.RoleExistsAsync("SYSTEMADMIN");
      if (!x)
      {
        var role = await _roleManager.CreateAsync(new IdentityRole { Name = "SYSTEMADMIN" });
      }
      var addRoleUser = await _userManager.AddToRoleAsync(user, "SYSTEMADMIN");
      if (!addRoleUser.Succeeded) return BadRequest(new ApiResponse(400, "Không thêm được role cho user"));
      var userIdentity = new TeacherDto
      {
        UserName = user.UserName,
        Email = user.Email,
        DisplayName = user.DisplayName,
        PhoneNumber = register.PhoneNumber,
        Address = register.Address,
      };
      var resultRegister = new
      {
        Message = "Success Register",
        data = userIdentity,
        Success = true,
      };
      return Ok(resultRegister);
    }
    [HttpPost("login")]
    public async Task<ActionResult<UserDto>> Login(LoginDto login)
    {
      try
      {
        Console.WriteLine(login.UserName);
        Console.WriteLine(login.Password);
        var currentUser = _userManager.FindByNameAsync(login.UserName);
        Console.WriteLine(currentUser.Result);
        if (currentUser.Result == null)
          return BadRequest(new ApiResponse(400, "Sai username hoặc password"));
        if (await _userManager.IsEmailConfirmedAsync(currentUser.Result) == false)
        {
          return BadRequest(new ApiResponse(400, "Email chưa được xác nhận"));
        }
        var roleUser = await _userManager.GetRolesAsync(currentUser.Result);
        var result = await _signInManager.CheckPasswordSignInAsync(currentUser.Result, login.Password, false);
        if (result.IsLockedOut) return BadRequest(new ApiResponse(400, "Tài khoản bạn đã bị khóa"));
        if (!result.Succeeded) return BadRequest(new ApiResponse(400, "Sai username hoặc password"));
        string tokenUser = _tokenService.CreateToken(currentUser.Result);
        if (currentUser.Result.urlAvatar != null)
        {
          var linkAvartar = _uploadService.GetLinkAvatar(currentUser.Result.urlAvatar);
          var userIdentity = new UserDto
          {
            UserName = currentUser.Result.UserName,
            DisplayName = currentUser.Result.DisplayName,
            PhoneNumber = currentUser.Result.PhoneNumber,
            Address = currentUser.Result.Address,
            Email = currentUser.Result.Email,
            Role = roleUser[0],
            Token = tokenUser,
            Avatar = linkAvartar.linkUrl
          };
          var resultLogin = new
          {
            Message = "Success Login",
            data = userIdentity,
            Success = true,
          };
          Console.WriteLine(resultLogin);
          return Ok(resultLogin);
        }
        else
        {
          var userIdentity = new UserDto
          {
            UserName = currentUser.Result.UserName,
            DisplayName = currentUser.Result.DisplayName,
            PhoneNumber = currentUser.Result.PhoneNumber,
            Address = currentUser.Result.Address,
            Email = currentUser.Result.Email,
            Role = roleUser[0],
            Token = tokenUser,
            Avatar = null
          };
          var resultLogin = new
          {
            Message = "Success Login",
            data = userIdentity,
            Success = true,
          };
          Console.WriteLine(resultLogin);
          return Ok(resultLogin);
        }

      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [HttpGet("emailexists")]
    public async Task<ActionResult<bool>> CheckEmailExistsAsync([FromQuery] string email)
    {
      return await _userManager.FindByEmailAsync(email) != null;
    }
    [HttpGet("usernameexists")]
    public async Task<ActionResult<bool>> CheckUserNameAsync([FromQuery] string username)
    {
      return await _userManager.FindByNameAsync(username) != null;
    }
    [HttpPost("resetpass")]
    public async Task<ActionResult<UserDto>> ResetPassword([FromBody] ResetPasswordDto resetPasswordDto)
    {
      try
      {
        Console.WriteLine(resetPasswordDto.NewPassword);
        var user = await _userManager.FindByEmailAsync(resetPasswordDto.email);
        Console.WriteLine(user);
        Console.WriteLine(resetPasswordDto.token);
        if (user == null)
          return NotFound();
        if (resetPasswordDto.NewPassword != resetPasswordDto.ConfirmNewPassword)
        {
          return BadRequest(new ApiResponse(400, "Mật khẩu mới và mật khẩu xác nhận không trùng khớp"));
        }
        var changePass = await _userManager.ResetPasswordAsync(user, resetPasswordDto.token, resetPasswordDto.NewPassword);
        Console.WriteLine(changePass.Errors);
        if (!changePass.Succeeded)
        {
          return BadRequest(new ApiResponse(400, "Mật khẩu không hợp lệ"));
        }
        return Ok("Change password success");
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [HttpPost("ForgetPassword")]
    public async Task<ActionResult<UserDto>> ForgetPassword(ForgetPasswordDto forgot)
    {
      var user = await _userManager.FindByEmailAsync(forgot.Email);
      if (user == null)
      {
        return BadRequest(new ApiResponse(404, "Không tìm thấy email"));
      }
      Console.WriteLine(user);
      if (user == null)
        return NotFound();
      var confirmationToken = await _userManager.GeneratePasswordResetTokenAsync(user);
      Console.WriteLine(confirmationToken);
      string confirmationLink = $"http://localhost:3000/doimatkhaukhiquen?email={user.Email}&token={confirmationToken}";
      Console.WriteLine("Link reset");
      Console.WriteLine(confirmationLink);
      SendEmail(forgot.Email, "Forgot Password", confirmationLink, "Lấy lại mật khẩu ở LMS Learning");
      var userIdentity = new
      {
        Email = forgot.Email,
      };
      var resultSuccess = new
      {
        Message = "Send Email Success",
        data = userIdentity,
        Success = true,
      };
      return Ok(resultSuccess);
    }
    [HttpGet]
    public async Task<IActionResult> ConfirmEmail(string token, string email)
    {
      var user = await _userManager.FindByEmailAsync(email);

      if (user == null)
        return BadRequest(new ApiResponse(400, "Xác nhận không thành công"));
      var result = await _userManager.ConfirmEmailAsync(user, token);

      if (!result.Succeeded) return BadRequest(new ApiResponse(400, "Xác nhận thất bại"));
      var resultSuccess = new
      {
        Message = "Kích hoạt tài khoản thành công",
        Success = true,
      };

      return Ok(resultSuccess);
    }
    [HttpPost("SendEmail")]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public void SendEmail(string email, string subject, string htmlMessage, string content)
    {
      string fromMail = "lmsneithgroup01@gmail.com";
      string fromPassword = "NhutThien123";

      MailMessage message = new MailMessage();
      message.From = new MailAddress(fromMail);
      message.Subject = subject;
      message.To.Add(new MailAddress(email));
      if (htmlMessage != "")
      {
        message.Body = content + $"<p>Nhấn vào link thực hiện yêu cầu <a href='{htmlMessage}'>Click here</a></p>";
      }
      else
      {
        message.Body = content;
      }
      message.IsBodyHtml = true;

      var smtpClient = new SmtpClient("smtp.gmail.com")
      {
        Port = 587,
        Credentials = new NetworkCredential(fromMail, fromPassword),
        EnableSsl = true,
      };
      smtpClient.Send(message);
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPut("updateUser")]
    public async Task<ActionResult<UserDto>> UpdateUser([FromForm] UpdateUserDto updateUser)
    {
      try
      {
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        if (updateUser.avatar != null)
        {
          var uploadAvatar = await _uploadService.UploadAvatar(user.Id, updateUser.avatar);
          if (uploadAvatar == null)
          {
            return BadRequest(new ApiResponse(400, "Update Failed Infomation"));
          }
          user.DisplayName = updateUser.DisplayName;
          user.PhoneNumber = updateUser.PhoneNumber;
          user.Address = updateUser.Address;
          user.urlAvatar = uploadAvatar.nameFile;
          var updateUserNew = await _userManager.UpdateAsync(user);
          var result = new
          {
            DisPlayName = user.DisplayName,
            PhoneNumber = user.PhoneNumber,
            Address = user.Address,
            UrlAvatar = uploadAvatar.url
          };
          var resultUpdateInformation = new
          {
            Message = "Success Update Information",
            data = result,
            Success = true,
          };
          return Ok(resultUpdateInformation);
        }
        else
        {
          user.DisplayName = updateUser.DisplayName;
          user.PhoneNumber = updateUser.PhoneNumber;
          user.Address = updateUser.Address;
          var updateUserNew = await _userManager.UpdateAsync(user);
          var getLink = _uploadService.GetLinkAvatar(user.urlAvatar);
          var result = new
          {
            DisPlayName = user.DisplayName,
            PhoneNumber = user.PhoneNumber,
            Address = user.Address,
            UrlAvatar = getLink.linkUrl
          };
          var resultUpdateInformation = new
          {
            Message = "Success Update Information",
            data = result,
            Success = true,
          };
          return Ok(resultUpdateInformation);
        }
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }

    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPut("changepass")]
    public async Task<ActionResult<UserDto>> ChangePassword(ChangePasswordDto changepassword)
    {
      var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
      var token = await _userManager.GeneratePasswordResetTokenAsync(user);
      var result = await _userManager.ChangePasswordAsync(user, changepassword.Password, changepassword.NewPassword);
      if (!result.Succeeded)
      {
        return BadRequest(new ApiResponse(400, "Mật khẩu không hợp lệ"));

      }
      return Ok("Change password success");

    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("getUser")]
    public async Task<ActionResult<UserDto>> GetCurrentUser()
    {
      try
      {
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        if (user.urlAvatar != null)
        {
          var linkUrl = _uploadService.GetLinkAvatar(user.urlAvatar);
          var result = new
          {
            Message = "Get User Success",
            data = new
            {
              userId = user.Id,
              DisplayName = user.DisplayName,
              Address = user.Address,
              UserName = user.UserName,
              urlAvatar = linkUrl.linkUrl,
              Email = user.Email,
              Phone = user.PhoneNumber
            },
            Success = true,
          };
          return Ok(result);
        }
        else
        {
          var result = new
          {
            Message = "Get User Success",
            data = new
            {
              userId = user.Id,
              DisplayName = user.DisplayName,
              Address = user.Address,
              UserName = user.UserName,
              urlAvatar = user.urlAvatar,
              Email = user.Email,
              Phone = user.PhoneNumber
            },
            Success = true,
          };
          return Ok(result);
        }
      }
      catch
      {
        return BadRequest(new ApiResponse(400, "Xác nhận thất bại"));
      }
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpDelete("deleteUser/{idUser}")]
    public async Task<ActionResult<UserDto>> DeleteUser(string idUser)
    {
      try
      {
        var userCurrent = await _userManager.FindByIdAsync(idUser);
        if (userCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "User doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var roleUser = await _userManager.GetRolesAsync(user);
        if (roleUser[0] == "SYSTEMADMIN")
        {

          Console.WriteLine(userCurrent);
          var userResult = await _userManager.DeleteAsync(userCurrent);
          if (userResult != null)
          {
            var result = new
            {
              Message = "Delete User Success",
              Success = true,
            };
            return Ok(result);
          }
        }
        return BadRequest(new ApiResponse(400, "Delete Failed User"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("getAllUser")]
    public async Task<ActionResult<AppUser>> GetListUser([FromQuery] string searchRole, [FromQuery] string searchName, [FromQuery] int pageNumber, [FromQuery] int pageSize)
    {
      try
      {
        if (pageSize < 8 || pageSize > 20)
        {
          pageSize = 8;
        }
        if (pageNumber < 1)
        {
          pageNumber = 1;
        }
        if (searchName == null)
        {
          searchName = "";
        }
        if (searchRole == null)
        {
          searchRole = "";
        }
        var currentUser = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var userRole = await _userManager.GetRolesAsync(currentUser);
        if (userRole[0] == "SYSTEMADMIN" || userRole[0] == "INSTRUCTOR")
        {
          var usersName = await _userManager.Users.Where(x => x.DisplayName.Contains(searchName)).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();
          List<UserBackDto> resultUserName = _mapper.Map<List<UserBackDto>>(usersName);
          var usersRole = new List<AppUser>();
          for (int i = 0; i < resultUserName.Count(); i++)
          {
            var role = await _userManager.GetRolesAsync(usersName[i]);
            resultUserName[i].role = role[0];
            if (usersName[i].urlAvatar != null)
            {
              var avatar = _uploadService.GetLinkAvatar(usersName[i].urlAvatar);
              resultUserName[i].avatar = avatar.linkUrl;
            }
            Console.WriteLine(role[0]);
            if (role[0] == searchRole)
            {
              usersRole.Add(usersName[i]);
            }
          }
          List<UserBackDto> resultUserRole = _mapper.Map<List<UserBackDto>>(usersRole);
          for (int i = 0; i < resultUserRole.Count(); i++)
          {
            var role = await _userManager.GetRolesAsync(usersRole[i]);
            if (usersRole[i].urlAvatar != null)
            {
              var avatar = _uploadService.GetLinkAvatar(usersRole[i].urlAvatar);
              resultUserRole[i].avatar = avatar.linkUrl;
            }
            resultUserRole[i].role = role[0];
          }
          if (searchRole != "")
          {
            var result = new
            {
              Message = "Get User success",
              data = resultUserRole,
              Success = true,
              statusCode = 200
            };
            return Ok(result);
          }
          else
          {
            var result = new
            {
              Message = "Get User success",
              data = resultUserName,
              Success = true,
              statusCode = 200
            }; return Ok(result);
          }
        }
        else
        {
          return BadRequest(new ApiResponse(400, "Get All User Failed"));
        }
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPut("updateAccountUser")]
    public async Task<ActionResult<UserDto>> UpdateAccountUser(AccountDto accountDto)
    {
      try
      {
        var currentUser = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var userRole = await _userManager.GetRolesAsync(currentUser);
        if (userRole[0] != "SYSTEMADMIN")
        {
          return BadRequest(new ApiResponse(400, "Update User Failed"));
        }
        var accountUpdate = await _userManager.FindByIdAsync(accountDto.id);
        if (accountUpdate == null)
        {
          return BadRequest(new ApiResponse(400, "Update User Failed"));
        }

        var token = await _userManager.GeneratePasswordResetTokenAsync(accountUpdate);
        var changePass = await _userManager.ResetPasswordAsync(accountUpdate, token, accountDto.NewPassword);
        SendEmail(accountUpdate.Email, "Reset PassWord", "", $"SystemAdmin đã reset pass cho bạn thành {accountDto.NewPassword}");
        var resultUpdateInformation = new
        {
          Message = "Change PassWord Account Success",
          Success = true,
        };
        return Ok(resultUpdateInformation);
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }

    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

    [HttpPost("LockUser")]
    public async Task<ActionResult<UserDto>> LockUser([FromQuery] string idUser, [FromQuery] int day)
    {
      try
      {
        var currentUser = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var userRole = await _userManager.GetRolesAsync(currentUser);
        if (userRole[0] != "SYSTEMADMIN")
        {
          return BadRequest(new ApiResponse(400, "Update User Failed"));
        }
        var user = await _userManager.FindByIdAsync(idUser);
        if (user == null)
        {
          return BadRequest(new ApiResponse(404));

        }
        var lockdate = DateTime.Now.AddDays(day);
        Console.WriteLine(lockdate);
        var check = await _userManager.SetLockoutEndDateAsync(user, lockdate);
        var result = new
        {
          Message = "Lock User success",
          data = user,
          Success = true,
          statusCode = 200
        };
        return Ok(result);
      }
      catch
      {
        return BadRequest(new ApiResponse(500));
      }
    }
  }
}
