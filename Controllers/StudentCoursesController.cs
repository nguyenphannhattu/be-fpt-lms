using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using API.Extensions;
using AutoMapper;
using Core.Entities;
using Core.Entities.Identity;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace identity.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentCoursesController : ControllerBase
    {
        private readonly IStudentCoursesRepository _studentCourseRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly IStudentScoresRepository _studentScores;
        private readonly UserManager<AppUser> _userManager;
        private readonly IMapper _mapper;
        private readonly IUploadService _uploadService;

        private readonly IMentorCoursesRepository _mentorCoursesRepository;

        public StudentCoursesController(IStudentCoursesRepository studentCoursesRepository, IStudentScoresRepository studentScoresRepository, UserManager<AppUser> userManager, ICourseRepository courseRepository, IMapper mapper, IMentorCoursesRepository mentorCoursesRepository, IUploadService uploadService)
        {
            _courseRepository = courseRepository;
            _studentCourseRepository = studentCoursesRepository;
            _studentScores = studentScoresRepository;
            _userManager = userManager;
            _mapper = mapper;
            _mentorCoursesRepository = mentorCoursesRepository;
            _uploadService = uploadService;
        }

        [HttpGet("getStudentCourse")]
        public async Task<ActionResult<StudentCourses>> GetStudentCourse([FromQuery] string idCourse, [FromQuery] string idStudent)
        {
            try
            {
                var studentCourses = await _studentCourseRepository.FindByIdCourseAndIdUser(idCourse, idStudent);
                if (studentCourses == null)
                {
                    return BadRequest(new ApiResponse(404, "StudentCourses doesn't exist"));
                }
                var result = new
                {
                    Message = "Get StudentCourse Success",
                    data = studentCourses,
                    Success = true,
                };
                return Ok(result);
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }

        }

        [HttpPost("SendEmail")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public void SendEmail(string email, string subject, string content)
        {
            string fromMail = "lmsneithgroup01@gmail.com";
            string fromPassword = "NhutThien123";

            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromMail);
            message.Subject = subject;
            message.To.Add(new MailAddress(email));
            message.Body = content;
            message.IsBodyHtml = true;

            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential(fromMail, fromPassword),
                EnableSsl = true,
            };
            smtpClient.Send(message);
        }

        [HttpGet("getTotalOfStudents/{status}")]
        public async Task<ActionResult<int>> GetTotalOfStudents(string status, [FromQuery] string idCourse)
        {
            try
            {
                var totalOfStudents = await _studentCourseRepository.TotalOfStudents(status, idCourse);
                var result = new
                {
                    Message = "Get Total Of Students " + status + " Success",
                    data = totalOfStudents,
                    Success = true,
                };
                return Ok(result);
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("getListStudentCourses/{status}")]
        public async Task<ActionResult<List<StudentCourses>>> GetListStudentCourses([FromQuery] string idCourse, string status, [FromQuery] int pageNumber, [FromQuery] int pageSize)
        {
            try
            {
                if (pageSize < 8 || pageSize > 20)
                {
                    pageSize = 8;
                }
                if (pageNumber < 1)
                {
                    pageNumber = 1;
                }
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleUser = await _userManager.GetRolesAsync(user);
                var courses = await _courseRepository.FindById(idCourse);
                if ((roleUser[0] == "TEACHER" && user.Id == courses.idTeacher)
                || (roleUser[0] == "INSTRUCTOR" && user.Id == courses.idInstructor)
                || (roleUser[0] == "CLASSADMIN" && user.Id == courses.idClassAdmin))
                {
                    var StudentCourses = await _studentCourseRepository.GetListStudentCourseAsync(idCourse, status, pageSize, pageNumber);
                    var mentorCourses = await _mentorCoursesRepository.FindByIdCourse(idCourse, 10, 1);
                    List<StudentCoursesAVGDto> studentCoursesAvg = _mapper.Map<List<StudentCoursesAVGDto>>(StudentCourses);
                    foreach (var itemStudentCourseAvg in studentCoursesAvg)
                    {
                        itemStudentCourseAvg.Student = await _userManager.FindByIdAsync(itemStudentCourseAvg.idUser);
                        itemStudentCourseAvg.aVGScore = await _studentScores.GetAVGScoreOfStudent(itemStudentCourseAvg.idCourse, itemStudentCourseAvg.idUser);
                        foreach (var itemMentorCourse in mentorCourses)
                        {
                            if (itemStudentCourseAvg.idUser == itemMentorCourse.idUser)
                            {
                                itemStudentCourseAvg.isMentor = true;
                            }
                        }
                        if (itemStudentCourseAvg.Student.urlAvatar != null)
                        {
                            var resultGetLinkAvatar = _uploadService.GetLinkAvatar(itemStudentCourseAvg.Student.urlAvatar);
                            itemStudentCourseAvg.Student.urlAvatar = resultGetLinkAvatar.linkUrl;
                        }
                    }
                    var result = new
                    {
                        Message = "Get List StudentCourses Success",
                        data = studentCoursesAvg,
                        Success = true,
                    };
                    return Ok(result);
                }
                else
                {
                    return BadRequest(new ApiResponse(400, "Bạn không có quyền truy cập!"));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("createStudentCourses")]
        public async Task<ActionResult<StudentCourses>> CreateStudentCourses(StudentCoursesDto studentCoursesDto)
        {
            try
            {
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                var roleStudent = await _userManager.GetRolesAsync(user);
                Console.WriteLine(roleStudent[0]);
                if (roleStudent[0] == "STUDENT")
                {
                    var checkStudentCourses = await _studentCourseRepository.FindByIdCourseAndIdUser(studentCoursesDto.idCourse, user.Id);
                    if (checkStudentCourses != null)
                        return BadRequest(new ApiResponse(400, "Bạn đã đăng kí khóa học này rồi"));
                    var studentCourses = new StudentCourses()
                    {
                        idCourse = studentCoursesDto.idCourse,
                        idUser = user.Id,
                        status = "AWAIT",
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now,
                    };
                    var studentCoursesResult = await _studentCourseRepository.CreateStudentCourse(studentCourses);
                    if (studentCoursesResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Create StudentCourses Success",
                            data = studentCourses,
                            Success = true,
                        };
                        return Ok(result);
                    }
                }
                return BadRequest(new ApiResponse(400, "Create Failed StudentCourses"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("activateStudentCourses")]
        public async Task<ActionResult<StudentCourses>> ActivateStudentCourse(StudentCoursesDto studentCoursesDto)
        {
            try
            {
                var coursesCurrent = await _courseRepository.FindById(studentCoursesDto.idCourse);
                var studentCoursesCurrent = await _studentCourseRepository.FindById(studentCoursesDto.idCourse, studentCoursesDto.idStudent);
                var student = await _userManager.FindByIdAsync(studentCoursesDto.idStudent);
                Console.WriteLine(studentCoursesCurrent);
                if (studentCoursesCurrent == null)
                {
                    return BadRequest(new ApiResponse(404, "StudentCourses doesn't exist"));
                }
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);

                var classAdmin = await _userManager.FindByIdAsync(user.Id);
                var roleClassAdmin = await _userManager.GetRolesAsync(user);
                if (roleClassAdmin[0] == "CLASSADMIN" && user.Id == coursesCurrent.idClassAdmin)
                {
                    studentCoursesCurrent.idCourse = studentCoursesDto.idCourse;
                    studentCoursesCurrent.idUser = studentCoursesDto.idStudent;
                    studentCoursesCurrent.status = "STUDYING";
                    studentCoursesCurrent.updatedAt = DateTime.Now;
                    var StudentCoursesResult = await _studentCourseRepository.UpdateStudentCourse(studentCoursesCurrent);
                    string content = "Bạn đã được xác nhận vào khóa học: " + coursesCurrent.name;
                    string subject = "Tham Gia Khóa Học";

                    SendEmail(student.Email, subject, content);

                    if (StudentCoursesResult == "Success")
                    {
                        var result = new
                        {
                            Message = "Update StudentCourses Success",
                            data = studentCoursesCurrent,
                            Success = true,
                        };
                        return Ok(result);
                    }
                    else if (StudentCoursesResult == "StudentCourses doesn't exist")
                    {
                        return BadRequest(new ApiResponse(404, "StudentCourses doesn't exist"));
                    }
                }

                return BadRequest(new ApiResponse(400, "Update Failed StudentCourses"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpDelete("cancelStudentCourses")]
        public async Task<ActionResult<StudentCourses>> CancelStudentCourse(StudentCoursesDto studentCoursesDto)
        {
            try
            {
                var coursesCurrent = await _courseRepository.FindById(studentCoursesDto.idCourse);
                var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
                if (studentCoursesDto.idStudent == null)
                {
                    var studentCoursesCurrent = await _studentCourseRepository.FindByIdCourseAndIdUser(studentCoursesDto.idCourse, user.Id);
                    if (studentCoursesCurrent == null)
                    {
                        return BadRequest(new ApiResponse(404, "StudentCourses doesn't exist"));
                    }
                    var roleStudent = await _userManager.GetRolesAsync(user);
                    if (roleStudent[0] == "STUDENT")
                    {
                        var StudentCoursesResult = await _studentCourseRepository.DeleteStudentCourse(studentCoursesDto.idCourse, user.Id);
                        string content = "Bạn đã thoát khỏi khóa học: " + coursesCurrent.name;
                        SendEmail(user.Email, "Hủy Khóa Học", content);

                        if (StudentCoursesResult == "Success")
                        {
                            var result = new
                            {
                                Message = "Delete StudentCourses Success",
                                data = studentCoursesCurrent,
                                Success = true,
                            };
                            return Ok(result);
                        }
                        else if (StudentCoursesResult == "StudentCourses doesn't exist")
                        {
                            return BadRequest(new ApiResponse(404, "StudentCourses doesn't exist"));
                        }
                    }
                }
                else
                {
                    var studentCoursesCurrent = await _studentCourseRepository.FindByIdCourseAndIdUser(studentCoursesDto.idCourse, studentCoursesDto.idStudent);
                    var student = await _userManager.FindByIdAsync(studentCoursesDto.idStudent);
                    if (studentCoursesCurrent == null)
                    {
                        return BadRequest(new ApiResponse(404, "StudentCourses doesn't exist"));
                    }
                    var classAdmin = await _userManager.FindByIdAsync(user.Id);
                    var roleClassAdmin = await _userManager.GetRolesAsync(user);
                    if (roleClassAdmin[0] == "CLASSADMIN" && user.Id == coursesCurrent.idClassAdmin)
                    {
                        var StudentCoursesResult = await _studentCourseRepository.DeleteStudentCourse(studentCoursesDto.idCourse, studentCoursesDto.idStudent);
                        string content = "ClassAdmin đã mời bạn thoát khỏi khóa học: " + coursesCurrent.name;
                        string subject = "Hủy Khóa Học";

                        SendEmail(student.Email, subject, content);

                        if (StudentCoursesResult == "Success")
                        {
                            var result = new
                            {
                                Message = "Delete StudentCourses Success",
                                data = studentCoursesCurrent,
                                Success = true,
                            };
                            return Ok(result);
                        }
                        else if (StudentCoursesResult == "StudentCourses doesn't exist")
                        {
                            return BadRequest(new ApiResponse(404, "StudentCourses doesn't exist"));
                        }
                    }
                }
                return BadRequest(new ApiResponse(400, "Update Failed StudentCourses"));

            }
            catch (Exception e)
            {
                Console.WriteLine("Bat Exception: {0}", e);
                return BadRequest(new ApiResponse(500, "Internal Server"));
            }
        }




    }
}
