using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Extensions;
using AutoMapper;
using Core.Entities;
using Core.Entities.Identity;
using Core.Errors;
using Core.Interfaces;
using identity.Dto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace identity.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class CourseController : ControllerBase
  {
    private readonly ICourseRepository _courseRepository;
    private readonly UserManager<AppUser> _userManager;
    private readonly IRoadmapContentRepository _roadmapContentRepository;
    private readonly IRoadMapsRepository _roadmapRepository;
    private readonly ICreateAssigmentsRepository _createAssigmentsRepository;
    private readonly IStudentCoursesRepository _studentCourseRepository;
    private readonly IEveluateCourseRepository _eveluateCourseRepository;
    private readonly IUploadService _uploadService;
    private readonly IMapper _mapper;
    private readonly IMentorCoursesRepository _mentorCourseRepository;

    public CourseController(ICourseRepository courseRepository, UserManager<AppUser> userManager, IRoadMapsRepository roadmapRepository, IRoadmapContentRepository roadmapContentRepository, ICreateAssigmentsRepository createAssigmentsRepository, IStudentCoursesRepository studentCourseRepository, IEveluateCourseRepository eveluateCourseRepository, IUploadService uploadService, IMapper mapper, IMentorCoursesRepository mentorCoursesRepository)
    {
      _courseRepository = courseRepository;
      _userManager = userManager;
      _roadmapContentRepository = roadmapContentRepository;
      _roadmapRepository = roadmapRepository;
      _eveluateCourseRepository = eveluateCourseRepository;
      _createAssigmentsRepository = createAssigmentsRepository;
      _studentCourseRepository = studentCourseRepository;
      _uploadService = uploadService;
      _mentorCourseRepository = mentorCoursesRepository;
      _mapper = mapper;
    }

    [HttpGet("getInformationCourse/{id}")]
    public async Task<ActionResult<Courses>> GetInformationCourse(string id)
    {
      try
      {
        var courseCurrent = await _courseRepository.FindById(id);
        Console.WriteLine(courseCurrent);
        if (courseCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "Course doesn't exist"));
        }
        var getLinkURL = _uploadService.GetLinkAvatar(courseCurrent.URLImage);
        courseCurrent.URLImage = getLinkURL.linkUrl;
        var teacher = await _userManager.FindByIdAsync(courseCurrent.idTeacher);
        var instructor = await _userManager.FindByIdAsync(courseCurrent.idInstructor);
        var classadmin = await _userManager.FindByIdAsync(courseCurrent.idClassAdmin);
        var totalOfStudents = await _studentCourseRepository.TotalOfStudents("ALL", id);
        var listEveluateCurrent = await _eveluateCourseRepository.GetAllEveluateCoursesAsync(id);
        float totalPoint = 0;
        int totalEveluate = 0;
        foreach (var item in listEveluateCurrent)
        {
          totalPoint = totalPoint + item.point;
          totalEveluate = totalEveluate + 1;
        }
        totalPoint = (float)Math.Round(totalPoint / totalEveluate, 2);
        var eveluate = new
        {
          totalEveluate = totalEveluate,
          totalPoint = totalPoint
        };
        var result = new
        {
          Message = "Get Courses Success",
          course = courseCurrent,
          student = totalOfStudents,
          eveluate = eveluate,
          Success = true,
        };
        return Ok(result);
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("getDetailCourse/{id}")]
    public async Task<ActionResult<Courses>> GetDetailCourse(string id)
    {
      try
      {
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var roleUser = await _userManager.GetRolesAsync(user);
        var courses = await _courseRepository.FindById(id);
        var courseStudent = new StudentCourses();
        bool isMentor = false;
        if (courses == null)
        {
          return BadRequest(new ApiResponse(404, "course doesn't exist"));
        }
        if (roleUser[0] == "STUDENT")
        {
          courseStudent = await _studentCourseRepository.FindByIdCourseAndIdUser(id, user.Id);
          var mentorCourses = await _mentorCourseRepository.FindByIdCourseAndIdUser(courses.Id,user.Id);
          if(mentorCourses != null)
          {
            isMentor = true;
          }
        }
        if (user.Id == courses.idClassAdmin || user.Id == courses.idInstructor || user.Id == courses.idTeacher || courseStudent.status == "STUDYING"|| courseStudent.status =="DONE")
        {
          var getLinkURL = _uploadService.GetLinkAvatar(courses.URLImage);
          courses.URLImage = getLinkURL.linkUrl;
          //Lấy thông tin RoadMapContent
          var listRoadMap = await _roadmapRepository.GetAllRoadMapsOfCourse(id);
          foreach (var item in listRoadMap)
          {
            item.Course = null;
          }
          var result = new
          {
            Message = "Get Courses Success",
            courses = courses,
            data = listRoadMap,
            isMentor = isMentor,
            Success = true,
          };
          return Ok(result);
        }
        else
        {
          var result = new
          {
            Message = "Get Courses Success",
            data = courses,
            Success = true,
          };
          return Ok(result);
        }
        // return BadRequest(new ApiResponse(400, "Get Failed Course"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }

    }
    [HttpGet("searchCourse")]
    public async Task<ActionResult<List<Courses>>> FindSearch([FromQuery] string searchCourse, [FromQuery] int pageNumber, [FromQuery] int pageSize)
    {
      if (pageSize < 8 || pageSize > 20)
      {
        pageSize = 8;
      }
      if (pageNumber < 1)
      {
        pageNumber = 1;
      }
      if (searchCourse == null)
      {
        Console.WriteLine("searchCourse");
        searchCourse = "";
      }
      var courses = await _courseRepository.FindByName(searchCourse, pageNumber, pageSize);
      List<CourseBackDto> resultCourses = _mapper.Map<List<CourseBackDto>>(courses);
      for (int i = 0; i < resultCourses.Count(); i++)
      {
        var getLinkURL = _uploadService.GetLinkAvatar(courses[i].URLImage);
        resultCourses[i].URLLink = getLinkURL.linkUrl;
      }
      var result = new
      {
        Message = "Get Courses Success",
        data = resultCourses,
        Success = true,
      };
      return Ok(result);
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPost("createCourse")]
    public async Task<ActionResult<Courses>> CreateCourse([FromForm] CourseDto courseDto)
    {
      try
      {
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var teacher = await _userManager.FindByIdAsync(courseDto.idTeacher);
        var classAdmin = await _userManager.FindByIdAsync(courseDto.idClassAdmin);
        var roleInstructor = await _userManager.GetRolesAsync(user);
        var roleTeacher = await _userManager.GetRolesAsync(teacher);
        var roleClassAdmin = await _userManager.GetRolesAsync(classAdmin);
        if (roleTeacher[0] == "TEACHER" && roleClassAdmin[0] == "CLASSADMIN" && roleInstructor[0] == "INSTRUCTOR")
        {
          Guid id = Guid.NewGuid();
          var resultUpload = await _uploadService.UploadImageCourse(id.ToString(), courseDto.URLImage);
          var course = new Courses()
          {
            Id = id.ToString(),
            idClassAdmin = classAdmin.Id,
            idInstructor = user.Id,
            idTeacher = teacher.Id,
            name = courseDto.name,
            status = "ACTIVE",
            description = courseDto.description,
            duration = courseDto.duration,
            createdAt = DateTime.Now,
            updatedAt = DateTime.Now,
            URLImage = resultUpload.nameFile
          };
          var courseResult = await _courseRepository.CreateCourse(course);
          var resultData = new
          {
            Id = id.ToString(),
            idClassAdmin = classAdmin.Id,
            idInstructor = user.Id,
            idTeacher = teacher.Id,
            name = courseDto.name,
            status = "ACTIVE",
            description = courseDto.description,
            duration = courseDto.duration,
            createdAt = DateTime.Now,
            updatedAt = DateTime.Now,
            urlLink = resultUpload.url,
          };
          if (courseResult == "Success")
          {
            var result = new
            {
              Message = "Create Course Success",
              data = resultData,
              Success = true,
            };
            return Ok(result);
          }
        }
        return BadRequest(new ApiResponse(400, "Create Failed Course"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpPut("updateCourse/{id}")]
    public async Task<ActionResult<Courses>> UpdateCourse(string id, [FromForm] CourseDto courseDto)
    {
      try
      {
        var courseCurrent = await _courseRepository.FindById(id);
        Console.WriteLine(courseCurrent);
        if (courseCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "course doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        if (user.Id == courseCurrent.idInstructor)
        {
          if (courseDto.URLImage != null)
          {
            var teacher = await _userManager.FindByIdAsync(courseDto.idTeacher);
            var classAdmin = await _userManager.FindByIdAsync(courseDto.idClassAdmin);
            var roleTeacher = await _userManager.GetRolesAsync(teacher);
            var roleInstructor = await _userManager.GetRolesAsync(user);
            var roleClassAdmin = await _userManager.GetRolesAsync(classAdmin);
            var resultUpload = await _uploadService.UploadImageCourse(id.ToString(), courseDto.URLImage);
            if (roleTeacher[0] == "TEACHER" && roleClassAdmin[0] == "CLASSADMIN" && roleInstructor[0] == "INSTRUCTOR")
            {
              courseCurrent.idClassAdmin = classAdmin.Id;
              courseCurrent.idTeacher = teacher.Id;
              courseCurrent.name = courseDto.name;
              courseCurrent.description = courseDto.description;
              courseCurrent.duration = courseDto.duration;
              courseCurrent.createdAt = courseCurrent.createdAt;
              courseCurrent.updatedAt = DateTime.Now;
              courseCurrent.URLImage = resultUpload.nameFile;
              var courseResult = await _courseRepository.UpdateCourse(courseCurrent);
              var resultData = new
              {
                Id = courseCurrent.Id,
                idClassAdmin = classAdmin.Id,
                idInstructor = courseCurrent.idInstructor,
                idTeacher = teacher.Id,
                name = courseDto.name,
                status = courseCurrent.status,
                description = courseDto.description,
                duration = courseDto.duration,
                createdAt = courseCurrent.createdAt,
                updatedAt = DateTime.Now,
                urlLink = resultUpload.url,
              };
              if (courseResult == "Success")
              {
                var result = new
                {
                  Message = "Update Course Success",
                  data = resultData,
                  Success = true,
                };
                return Ok(result);
              }
              else if (courseResult == "course doesn't exist")
              {
                return BadRequest(new ApiResponse(404, "course doesn't exist"));
              }
            }
          }
          else
          {
            var teacher = await _userManager.FindByIdAsync(courseDto.idTeacher);
            var classAdmin = await _userManager.FindByIdAsync(courseDto.idClassAdmin);
            var roleTeacher = await _userManager.GetRolesAsync(teacher);
            var roleInstructor = await _userManager.GetRolesAsync(user);
            var roleClassAdmin = await _userManager.GetRolesAsync(classAdmin);
            var getLinkURL = _uploadService.GetLinkAvatar(courseCurrent.URLImage);
            if (roleTeacher[0] == "TEACHER" && roleClassAdmin[0] == "CLASSADMIN" && roleInstructor[0] == "INSTRUCTOR")
            {
              courseCurrent.idClassAdmin = classAdmin.Id;
              courseCurrent.idTeacher = teacher.Id;
              courseCurrent.name = courseDto.name;
              courseCurrent.description = courseDto.description;
              courseCurrent.duration = courseDto.duration;
              courseCurrent.createdAt = courseCurrent.createdAt;
              courseCurrent.updatedAt = DateTime.Now;
              var courseResult = await _courseRepository.UpdateCourse(courseCurrent);
              var resultData = new
              {
                Id = courseCurrent.Id,
                idClassAdmin = classAdmin.Id,
                idInstructor = courseCurrent.idInstructor,
                idTeacher = teacher.Id,
                name = courseDto.name,
                status = courseCurrent.status,
                description = courseDto.description,
                duration = courseDto.duration,
                createdAt = courseCurrent.createdAt,
                updatedAt = DateTime.Now,
                urlLink = getLinkURL.linkUrl,
              };
              if (courseResult == "Success")
              {
                var result = new
                {
                  Message = "Update Course Success",
                  data = resultData,
                  Success = true,
                };
                return Ok(result);
              }
              else if (courseResult == "course doesn't exist")
              {
                return BadRequest(new ApiResponse(404, "course doesn't exist"));
              }
            }
          }

        }
        return BadRequest(new ApiResponse(400, "Update Failed Course"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("doneCourse/{id}")]
    public async Task<ActionResult<Courses>> DoneCourse(string id)
    {
      try
      {
        var courseCurrent = await _courseRepository.FindById(id);
        if (courseCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "course doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        if (user.Id == courseCurrent.idInstructor)
        {
          var roleUser = await _userManager.GetRolesAsync(user);
          courseCurrent.status = "DONE";
          if (roleUser[0] == "TEACHER" || roleUser[0] == "INSTRUCTOR")
          {
            var listStudent = await _studentCourseRepository.FindByIdCourse(id);
            foreach (var item in listStudent)
            {
              item.status = "DONE";
              await _studentCourseRepository.UpdateStudentCourse(item);
            }
            var check = await _mentorCourseRepository.DeleteMultipleMentorCourse(id);
            Console.WriteLine(check);
            var courseResult = await _courseRepository.UpdateCourse(courseCurrent);
            
            if (courseResult == "Success" && check == "Success")
            {
              var result = new
              {
                Message = "Update Course Success",
                data = courseCurrent,
                Success = true,
              };
              return Ok(result);
            }
            else if (courseResult == "course doesn't exist")
            {
              return BadRequest(new ApiResponse(404, "course doesn't exist"));
            }
          }
        }
        return BadRequest(new ApiResponse(400, "Update Failed Course"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("openAgainCourse/{id}")]
    public async Task<ActionResult<Courses>> OpenAgain(string id)
    {
      try
      {
        var courseCurrent = await _courseRepository.FindById(id);
        if (courseCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "course doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var roleUser = await _userManager.GetRolesAsync(user);
        if (roleUser[0] == "INSTRUCTOR")
        {
          courseCurrent.status = "ACTIVE";
          courseCurrent.createdAt = DateTime.Now;
          courseCurrent.updatedAt = DateTime.Now;
          var courseResult = await _courseRepository.UpdateCourse(courseCurrent);
          if (courseResult == "Success")
          {
            var result = new
            {
              Message = "Open Again Course Success",
              data = courseCurrent,
              Success = true,
            };
            return Ok(result);
          }
          else if (courseResult == "course doesn't exist")
          {
            return BadRequest(new ApiResponse(404, "course doesn't exist"));
          }
        }
        return BadRequest(new ApiResponse(400, "Open Again Failed Course"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("studingCourse/{id}")]
    public async Task<ActionResult<Courses>> StudingCourse(string id)
    {
      try
      {
        var courseCurrent = await _courseRepository.FindById(id);
        if (courseCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "course doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var roleUser = await _userManager.GetRolesAsync(user);
        if (roleUser[0] == "CLASSADMIN" && courseCurrent.status == "ACTIVE" && courseCurrent.idClassAdmin == user.Id)
        {
          courseCurrent.status = "STUDYING";
          courseCurrent.updatedAt = DateTime.Now;
          var courseResult = await _courseRepository.UpdateCourse(courseCurrent);
          if (courseResult == "Success")
          {
            var result = new
            {
              Message = "Studying Course Success",
              data = courseCurrent,
              Success = true,
            };
            return Ok(result);
          }
          else if (courseResult == "course doesn't exist")
          {
            return BadRequest(new ApiResponse(404, "course doesn't exist"));
          }
        }
        return BadRequest(new ApiResponse(400, "Studying Failed Course"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpDelete("deleteCourse/{id}")]
    public async Task<ActionResult<Courses>> DeleteCourse(string id)
    {
      try
      {
        var courseCurrent = await _courseRepository.FindById(id);
        if (courseCurrent == null)
        {
          return BadRequest(new ApiResponse(404, "course doesn't exist"));
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var roleInstructor = await _userManager.GetRolesAsync(user);
        if (user.Id == courseCurrent.idInstructor && roleInstructor[0] == "INSTRUCTOR")
        {
          courseCurrent.status = "DELETED";
          courseCurrent.updatedAt = DateTime.Now;
          var courseResult = await _courseRepository.UpdateCourse(courseCurrent);
          if (courseResult == "Success")
          {
            var result = new
            {
              Message = "Delete Course Success",
              Success = true,
            };
            return Ok(result);
          }
        }
        return BadRequest(new ApiResponse(400, "Delete Failed Course"));
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("getHistoryCourse")]
    public async Task<ActionResult<List<Courses>>> GetHistoryCourse([FromQuery] int pageNumber, [FromQuery] int pageSize, [FromQuery] string status)
    {
      Console.WriteLine("abc");
      try
      {
        if (pageSize < 8 || pageSize > 20)
        {
          pageSize = 8;
        }
        if (pageNumber < 1)
        {
          pageNumber = 1;
        }
        var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
        var courseByUser = await _studentCourseRepository.GetListHistoryCourseAsync(status, user.Id, pageSize, pageNumber);
        Console.WriteLine("a", courseByUser);
        foreach (var item in courseByUser)
        {
          var course = await _courseRepository.FindById(item.idCourse);
          item.Course = course;
          item.Student = null;
        }
        var result = new
        {
          Message = "Get Hisotry Success",
          data = courseByUser,
          Success = true,
        };
        return Ok(result);
      }
      catch (Exception e)
      {
        Console.WriteLine("Bat Exception: {0}", e);
        return BadRequest(new ApiResponse(500, "Internal Server"));
      }
    }
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [HttpGet("searchHistoryCourse")]
    public async Task<ActionResult<List<Courses>>> FindHistory([FromQuery] string searchCourse, [FromQuery] string status, [FromQuery] int pageNumber, [FromQuery] int pageSize)
    {
      if (pageSize < 8 || pageSize > 20)
      {
        pageSize = 8;
      }
      if (pageNumber < 1)
      {
        pageNumber = 1;
      }
      var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
      var roleUser = await _userManager.GetRolesAsync(user);
      if (roleUser[0] == "STUDENT")
      {
        if (searchCourse == null)
        {
          searchCourse = "";
        }
        var courseByUser = await _studentCourseRepository.FindCourseHistory(status, user.Id, searchCourse, pageSize, pageNumber);
        foreach (var item in courseByUser)
        {
          var course = await _courseRepository.FindById(item.idCourse);
          var getLinkURL = _uploadService.GetLinkAvatar(course.URLImage);
          item.Course = course;
          item.Course.URLImage = getLinkURL.linkUrl;
          item.Student = null;
        }
        var result = new
        {
          Message = "Get Courses Success",
          data = courseByUser,
          Success = true,
        };
        return Ok(result);
      }
      else if (roleUser[0] == "INSTRUCTOR")
      {
        if (searchCourse == null)
        {
          searchCourse = "";
        }
        var courseByUser = await _courseRepository.FindCourseHistoryInstructor(status, user.Id, searchCourse, pageSize, pageNumber);
        foreach (var item in courseByUser)
        {
          var getLinkURL = _uploadService.GetLinkAvatar(item.URLImage);
          item.URLImage = getLinkURL.linkUrl;
          item.Instructor = null;
        }
        var result = new
        {
          Message = "Get Courses Success",
          data = courseByUser,
          Success = true,
        };
        return Ok(result);
      }
      else if (roleUser[0] == "TEACHER")
      {
        if (searchCourse == null)
        {
          searchCourse = "";
        }
        var courseByUser = await _courseRepository.FindCourseHistoryTeacher(status, user.Id, searchCourse, pageSize, pageNumber);
        foreach (var item in courseByUser)
        {
          var getLinkURL = _uploadService.GetLinkAvatar(item.URLImage);
          item.URLImage = getLinkURL.linkUrl;
          item.Instructor = null;
        }
        var result = new
        {
          Message = "Get Courses Success",
          data = courseByUser,
          Success = true,
        };
        return Ok(result);
      }
      else if (roleUser[0] == "CLASSADMIN")
      {
        if (searchCourse == null)
        {
          searchCourse = "";
        }
        var courseByUser = await _courseRepository.FindCourseHistoryClassAdmin(status, user.Id, searchCourse, pageSize, pageNumber);
        foreach (var item in courseByUser)
        {
          var getLinkURL = _uploadService.GetLinkAvatar(item.URLImage);
          item.URLImage = getLinkURL.linkUrl;
          item.Instructor = null;
        }
        var result = new
        {
          Message = "Get Courses Success",
          data = courseByUser,
          Success = true,
        };
        return Ok(result);
      }
        return BadRequest(new ApiResponse(400, "Get Failed Course"));
    }
  }
}
